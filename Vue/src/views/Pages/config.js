export default [
  /**
   * 数据格式
   * type 标记文件类型 markdown folder txt word
   * filename 文件名称
   * fileid 文件id
   * foldername 文件夹名称
   * folderid 文件夹id
   * parentfolderid 父级文件夹id
   * fileownerfolderid 文件所属文件夹id
   */
  {
    type: "markdown",
    filename: "测试文档1",
    suffix: "md",
    icon: "icon-file-markdown1",
  },
];
